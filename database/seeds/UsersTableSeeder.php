<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name'           	=> 'Predrag Vucinic',
                'email'          	=> 'voutchko@gmail.com',
                'password'       	=> Hash::make('temp1234'),
                'email_verified_at' => now(),
                'superuser' 	    => true
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
