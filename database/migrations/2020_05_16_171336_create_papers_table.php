<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->id();

            $table->string('name')->unique();
            $table->text('description')->nullable();

            $table->decimal('price_per_kg', 8, 2)->unsigned()->nullable();
            $table->decimal('price_per_sheet', 8, 2)->unsigned()->nullable();
            $table->string('specific_weight')->nullable();
            
            $table->bigInteger('base_sheet_id')->unsigned();
            $table->foreign('base_sheet_id')
                ->references('id')
                ->on('base_sheets')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papers');
    }
}
