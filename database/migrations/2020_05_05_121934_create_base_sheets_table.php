<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_sheets', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string('name')->unique();
            $table->decimal('x', 8, 2)->unsigned();
            $table->decimal('y', 8, 2)->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_sheets');
    }
}
