<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinePressSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_press_sheet', function (Blueprint $table) {
            
            $table->bigInteger('machine_id')->unsigned();
            $table->foreign('machine_id')
                ->references('id')
                ->on('machines')
                ->onDelete('cascade');
            
            $table->bigInteger('press_sheet_id')->unsigned();
            $table->foreign('press_sheet_id')
                ->references('id')
                ->on('press_sheets')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('machine_press_sheet', function (Blueprint $table) {
            
            Schema::dropIfExists('machine_press_sheet');
        });
    }
}
