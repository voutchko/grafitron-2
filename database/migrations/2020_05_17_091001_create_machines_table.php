<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->id();

            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->boolean('work_and_turn')->default(1);
            $table->integer('colour_no')->default(4);
            $table->integer('makulatur')->nullable();

            $table->decimal('plate_price', 5, 2)->unsigned()->nullable();
            $table->decimal('start_price', 8, 2)->unsigned()->nullable();
            $table->decimal('price_per_tausand', 8, 2)->unsigned()->nullable();
            $table->decimal('start_price_per_hour', 8, 2)->unsigned()->nullable();
            $table->decimal('price_per_hour', 8, 2)->unsigned()->nullable();
            $table->integer('start_time')->unsigned()->nullable();
            $table->decimal('sheets_per_hour', 8, 2)->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
