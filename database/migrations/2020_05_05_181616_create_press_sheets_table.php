<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePressSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('press_sheets', function (Blueprint $table) {
            $table->id();

            $table->string('name')->unique();
            $table->decimal('x', 8, 2)->unsigned();
            $table->decimal('y', 8, 2)->unsigned();

            $table->bigInteger('base_sheet_id')->unsigned();
            $table->foreign('base_sheet_id')
                ->references('id')
                ->on('base_sheets')
                ->onDelete('cascade');

            $table->integer('how_many')->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('press_sheets');
    }
}
