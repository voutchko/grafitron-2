<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            
            $table->id();

            $table->string('contact_name');
            $table->string('business_name')->nullable();;
            $table->text('address')->nullable();
            $table->text('notes')->nullable();
            $table->string('pib')->nullable()->unique();
            $table->string('mb')->nullable()->unique();
            $table->string('tel')->nullable();
            $table->string('email')->nullable()->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
