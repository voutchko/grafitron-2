(function ($) {
    'use strict';

    var pap = [];
    var sel = [];

    $('#select_plastic').on('changed.bs.select, change', function () {

        var $koji = $('#select_plastic option:selected').text();
        pap = getObjects(plastics, 'name', $koji);
        sel['opis'] = $koji;

        $('#select_sheet').find('option').not(':first').remove();
        $.each(pap[0].sheet_price, function(key, value) {
             $('#select_sheet')
                  .append($('<option>', { value : value.id })
                  .text(value.name));
        });
  
    });

    $('#select_sheet').on('changed.bs.select, change', function () {

        var $koji = $('#select_sheet option:selected').text();
        var pap2 = getObjects(pap[0].sheet_price, 'name', $koji);
        sel['tabak'] = $koji;
            

        $('#select_type').find('option').not(':first').remove();
        if(pap2[0].pivot.c_matt) {
            $('#select_type')
                  .append($('<option>', { value : pap2[0].pivot.c_matt })
                  .text('hladna mat'));
        }
        if(pap2[0].pivot.c_gloss) {
            $('#select_type')
                  .append($('<option>', { value : pap2[0].pivot.c_gloss })
                  .text('hladna sjajna'));
        }
        if(pap2[0].pivot.h_matt) {
            $('#select_type')
                  .append($('<option>', { value : pap2[0].pivot.h_matt })
                  .text('topla mat'));
        }
        if(pap2[0].pivot.h_gloss) {
            $('#select_type')
                  .append($('<option>', { value : pap2[0].pivot.h_gloss })
                  .text('topla sjajna'));
        }
        
    });

    $('#select_type').on('changed.bs.select, change', function () {

        sel['vrsta'] = $('#select_type option:selected').text();
        sel['cena'] = $('#select_type option:selected').val()

        $('#sheet').val(sel['tabak']);
        $('#description').val(sel['opis'] + ' / ' + sel['vrsta']);
        $('#price').val(sel['cena']);

        $('#plastic_coating').modal('hide');
  
    });


 

})(window.jQuery);

