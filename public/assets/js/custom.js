(function ($) {
    'use strict';
    /*---------------------------------  
        sticky header JS
    -----------------------------------*/
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 100) {
            $("#header-area").removeClass("sticky");
        } else {
            $("#header-area").addClass("sticky");
        }
    });
    /*---------------------------------  
        sticky header JS
    -----------------------------------*/
    /*======================================/
        menu-icon js
    ======================================*/
    $(".menu-icon").on('click', function (event) {
        $(".menu-icon").toggleClass("active");
    });
    $(".menu-icon").on('click', function (event) {
        $(".sidenav_menu").toggleClass("active");
    });


    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function (event) {
            $('#sidebar').toggleClass('active');
            $('#sidebarCollapse i').toggleClass('closed');
        });
    });

    $(document).ready(function () {
        
        $(".filters").on('click', function (event) {
            var $p = $('#tabak-1').data('tabak');
            $('#filters-'+$(this).data('tabak')).toggleClass('open');
            $('#tabak-'+$(this).data('tabak')).toggleClass('open');
        });

        $('.price_per_hour_no').on('click', function (event) {
            var $p = $(this).attr('id').replace("price_per_hour_no", "");
            $('#per_hour'+$p).prop('disabled', true);
            $('#per_sheet'+$p).prop('disabled', false);
        });

        $('.price_per_hour_yes').on('click', function (event) {
            var $p = $(this).attr('id').replace("price_per_hour_yes", "");
            $('#per_hour'+$p).prop('disabled', false);
            $('#per_sheet'+$p).prop('disabled', true);
        });

        $('.price_per_sheet_yes').on('click', function (event) {
            var $p = $(this).attr('id').replace("price_per_sheet_yes", "");
            $('#per_kg'+$p).prop('disabled', true);
            $('#per_sheet_p'+$p).prop('disabled', false);
        });

        $('.price_per_sheet_no').on('click', function (event) {
            var $p = $(this).attr('id').replace("price_per_sheet_no", "");
            $('#per_kg'+$p).prop('disabled', false);
            $('#per_sheet_p'+$p).prop('disabled', true);
        });

        $(".clickable-row").on('click', function (event) {
            window.location = $(this).data("href");
        });

        
    });


    /*---------------------------------
        page_scroll top JS
    --------------------------------*/
    $("a.page_scroll").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            //console.log($(hash).offset().top - topOffset);
            $('html, body').animate({
                scrollTop: $(hash).offset().top - $("header").outerHeight() + "px"
            }, 1200, function () {

                //window.location.hash = hash;
            });
        } // End if
    });
    /*======================================/
                  scroll top JS
    ======================================*/
    /*---------------------- 
        Scroll top js
    ------------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 100) {
            $('#scroll_up').fadeIn();
        } else {
            $('#scroll_up').fadeOut();
        }
    });
    $('#scroll_up').on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    /*---------------------- 
        Scroll top js
    ------------------------*/

    /*---------------------------------
        Bootstrap Selectpicker
    --------------------------------*/
    $('.selectpicker').selectpicker();
    /*---------------------------------
        Bootstrap Selectpicker
    --------------------------------*/

    /*---------------------------------
        Bootstrap Datepicker
    --------------------------------*/
    $('.datepicker').datepicker();
    /*---------------------------------
        Bootstrap Datepicker
    --------------------------------*/



})(window.jQuery);
///////////////////////////////////////////////////////////////////////////////////////////////


// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


//select on quota sheet
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}