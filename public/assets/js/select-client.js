(function ($) {
    'use strict';

    $('#select-client').on('changed.bs.select, change', function () {


        var $koji = $('#select-client').val();
        var pap = getObjects(clients, 'id', $koji);

        $('#client_contact_name').val(pap[0].contact_name);
        $('#client_business_name').val(pap[0].business_name);
        $('#client_address').val(pap[0].address);
        $('#pib').val(pap[0].pib);
        $('#client_tel').val(pap[0].tel);
        $('#client_email').val(pap[0].email);

    });

})(window.jQuery);