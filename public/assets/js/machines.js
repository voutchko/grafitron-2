(function ($) {
    'use strict';

    $('#select-paper-1').on('changed.bs.select, change', function () {

        var $koji = $('#select-paper-1').val();
        var pap = getObjects(papers, 'id', $koji);
        
        //alert(pap[0].base_sheet.x);

        $('#pp').val(pap[0].name);
        $('#ppd').val(pap[0].description);
        $('#prk').val(pap[0].pk);
        $('#prps').val(pap[0].ps);
        $('#pw').val(pap[0].w);
        $('#ppsx').val(pap[0].base_sheet.x);
        $('#ppsy').val(pap[0].base_sheet.y);

    });

})(window.jQuery);

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}