(function ($) {
    'use strict';

    $('.select_paper').on('changed.bs.select, change', function () {

        var $p = $(this).attr('id').replace("select_paper", "");
        var $koji = $('#select_paper'+$p+' option:selected').text();
        var pap = getObjects(papers, 'name', $koji);

        var $modal = $('#paper_weight');

        var selectValues = pap[0].w.split('|');

        if (selectValues.length > 1) {

            $('#select_paper_weght').find('option').not(':first').remove();
            $('#select_weight_paper_name').text($koji);

            $.each(selectValues, function(key, value) {
                 $('#select_paper_weght')
                      .append($('<option>', { value : key })
                      .text(value));
            });

            $modal.modal('show');

            $modal.on('hidden.bs.modal', function (e) {

                var $selected = $('.select_paper_weight option:selected').text();
                
                $('#pp'+$p).val(pap[0].name);
                $('#ppd'+$p).val(pap[0].description);
                $('#prk'+$p).val(pap[0].pk);
                $('#prps'+$p).val(pap[0].ps);
                $('#pw'+$p).val($selected);  //(pap[0].w);
                $('#pps'+$p).val(pap[0].base_sheet.name);
                $('#ppsx'+$p).val(pap[0].base_sheet.x);
                $('#ppsy'+$p).val(pap[0].base_sheet.y);
            })

        } else {

            $('#pp'+$p).val(pap[0].name);
            $('#ppd'+$p).val(pap[0].description);
            $('#prk'+$p).val(pap[0].pk);
            $('#prps'+$p).val(pap[0].ps);
            $('#pw'+$p).val(pap[0].w);
            $('#pps'+$p).val(pap[0].base_sheet.name);
            $('#ppsx'+$p).val(pap[0].base_sheet.x);
            $('#ppsy'+$p).val(pap[0].base_sheet.y);

        }
        
  
    });


    $('.select_paper_weight').on('changed.bs.select, change', function () {

        $('#paper_weight').modal('hide');
  
    });    

})(window.jQuery);