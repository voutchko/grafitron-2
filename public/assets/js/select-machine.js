(function ($) {
    'use strict';

    $('.select_machine').on('changed.bs.select, change', function () {

        var $p = $(this).attr('id').replace("select_machine", "");
        $('#per_hour'+$p).prop('disabled', true);

        var $koji = $('#select_machine'+$p+' option:selected').text();
        var mac = getObjects(machines, 'name', $koji);
        
        //alert(mac[0].name);

        $('#machine_name'+$p).val(mac[0].name);
        $('#machine_description'+$p).val(mac[0].description);
        $('#st'+$p).val(mac[0].st);
        $('#stsph'+$p).val(mac[0].stsph);
        $('#pph'+$p).val(mac[0].pph);
        $('#sph'+$p).val(mac[0].sph);
        $('#ppt'+$p).val(mac[0].ppt);
        $('#spr'+$p).val(mac[0].sp);
        $('#ppr'+$p).val(mac[0].pp);
        $('#mk'+$p).val(mac[0].mk);
        $('#ca'+$p).val(mac[0].cn);
        if(mac[0].wt == 1) $('#cb'+$p).val(mac[0].cn);

    });

})(window.jQuery);