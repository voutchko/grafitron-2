(function ($) {
    'use strict';

    $('.job-salary-type').on('change', function () {
        var $type = $(this).val();

        $('.salary-type-box').hide();

        $('.salary-type-' + $type).show();
    });

})(window.jQuery);