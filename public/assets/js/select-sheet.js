(function ($) {
    'use strict';

    $('.select_sheet').on('changed.bs.select, change', function () {

        var $p = $(this).attr('id').replace("select_sheet", "");
        var $koji = $('#select_sheet'+$p+' option:selected').text();
        var pap = getObjects(sheets, 'name', $koji);
        
        //alert(pap[0].base_sheet.x);

        $('#ps'+$p).val(pap[0].name);
        $('#psx'+$p).val(pap[0].x);
        $('#psy'+$p).val(pap[0].y);
        $('#hhm'+$p).val(pap[0].hm);

    });

})(window.jQuery);