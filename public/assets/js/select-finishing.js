(function ($) {
    'use strict';

    $('#select_finishing').on('changed.bs.select, change', function () {

        var $p = $(this).attr('id').replace("select_finishing", "");
        var $koji = $('#select_finishing'+$p+' option:selected').text();
        var pap = getObjects(finishings, 'name', $koji);
        
        //alert(pap[0].base_finishing.x);

        $('#name'+$p).val(pap[0].name);
        $('#description'+$p).val(pap[0].description);
        $('#start'+$p).val(pap[0].start);
        $('#per_pc'+$p).val(pap[0].per_pc);
        $('#per_thousand'+$p).val(pap[0].per_thousand);

    });

})(window.jQuery);