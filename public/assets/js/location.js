(function ($) {
    'use strict';

    var $locations = $('#location_ids');
    var $nameAsValue = $locations.data('name-as-value') || false;

    $('#area_ids').on('changed.bs.select, change', function () {
        var $areaIds = $(this).val();
        var $dataId = $(this).find('option:selected').data('id') || null;

        if (!$.isArray($areaIds)) {
            $areaIds = [$areaIds];
        }

        if ($dataId !== null) {
            $areaIds = [$dataId];
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/ajax/locations/get-by-area-ids-for-auth-user',
            data: {
                area_ids: $areaIds
            },
            success: function ($data) {
                $locations.html('');
                $.each($data, function ($value, $text) {

                    if ($nameAsValue) {
                        $value = $text;
                    }

                    $locations.append($('<option></option>').attr('value', $value).text($text));
                });

                if ($locations.hasClass('selectpicker')) {
                    $locations.selectpicker('refresh');
                }
            }
        });
    });

})(window.jQuery);