<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Vaša lozika je promenjena!',
    'sent' => 'Poslali smo vam link za resetovanje lozinke.',
    'token' => 'Ovaj token za promenu lozinke nije važeći.',
    'user' => "Ne možemo da nađemo korisnika sa tom mejl adresom.",
    'throttled' => 'Sačekajte malo pre sledećeg pokušaja.',

];
