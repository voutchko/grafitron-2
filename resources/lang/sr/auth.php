<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ovi podaci se ne slažu s našom bazom.',
    'throttle' => 'Previše pokušaja logovanja. Molimo vas pokušajte posle :seconds sekundi.',

];
