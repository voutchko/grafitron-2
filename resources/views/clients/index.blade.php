@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form class="row px-3">

            @csrf

            <div class="col">

            	<div class="row">
            		<div class="col">
            			
            			<h1 class="mt-5 mb-2">{{ __('Address book') }}</h1>

            			<h2>{{ __('Clients') }}</h2>
		            	<hr>

		            	@include('partials.alerts')

		            	<div>
					        <table class="ck" data-sort="table">
					            <thead>
					            <tr>
					            	<th class="header header-sort-{{ $by === 'contact_name' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'contact_name', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Contact') }}</a>
						            </th><th class="header header-sort-{{ $by === 'business_name' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'business_name', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Company') }}</a>
						            </th><th class="header header-sort-{{ $by === 'address' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'address', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Address') }}</a>
						            </th><th class="header header-sort-{{ $by === 'email' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'email', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">Email</a>
						            </th><th class="header header-sort-{{ $by === 'tel' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'tel', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Telephone') }}</a>
						            </th>
					                <th class="header">{{ __('Edit') }}</th>
					            </tr>
					            </thead>
					            <tbody>
					            	@if($clients->count())
					            		@foreach ($clients as $client)
							                <tr class="clickable-row" data-href="{{ route('clients.edit', $client->id) }}">
							                    <td>{{ $client->contact_name }}</td>
							                    <td>{{ $client->business_name }}</td>
							                    <td>{{ $client->address }}</td>
							                    <td>{{ $client->email }}</td>
							                    <td>{{ $client->tel }}</td>
							                    <td><i class="fa fa-caret-right"></i></td>
							                </tr>
							            @endforeach
						              @else
						              	<tr class="clickable-row" data-href="">
						                    <td colspan="6">{{ __('No entries') }}</td>
						                </tr>
						              @endif
					            </tbody>
					        </table>
					    </div>

					    <div class="pagination mt-5 w-100 align-items-center">

					    	{{ $clients->links() }}

					    </div>

            		</div>
            	</div>

            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <a class="btn btn-primary my-1 w-100" href="{{ route('clients.create') }}">{{ __('Add new client') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>

@endsection