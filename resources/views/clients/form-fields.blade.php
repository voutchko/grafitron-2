<div class="form-group row">
    <label for="contact_name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Contact') }}</label>
    <div class="col-lg-5">
        <input type="text" class="form-control form-control-sm {{ $errors->has('contact_name') ? 'is-invalid' : '' }}" id="contact_name" name="contact_name" placeholder="[{{ __('contact name') }}]" value="{{ old('contact_name', $client->contact_name ?? '') }}" required />
        <div class="invalid-feedback">
            @if ($errors->has('contact_name'))
                @foreach ($errors->get('contact_name') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ __('This field is mandatory!') }}
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="business_name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Company') }}</label>
    <div class="col-lg-5">
        <input type="text" class="form-control form-control-sm {{ $errors->has('business_name') ? 'is-invalid' : '' }}" id="business_name" name="business_name" placeholder="[{{ __('Company') }}]" value="{{ old('business_name', $client->business_name ?? '') }}" required />
        <div class="invalid-feedback">
            @if ($errors->has('business_name'))
               @foreach ($errors->get('business_name') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ __('This field is mandatory!') }}
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="address" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Address') }}</label>
    <div class="col-lg-5">
        <textarea type="text" class="form-control form-control-sm" id="address" name="address" rows="3" placeholder="[{{ __('Address') }}]">{{ old('address', $client->address ?? '') }}</textarea>
    </div>
</div>

<div class="form-group row">
    <label for="pib" class="col-lg-2 col-form-label col-form-label-sm">{{ __('VAT number') }}</label>
    <div class="col-lg-5">
        <input type="number" step="1" class="form-control form-control-sm {{ $errors->has('pib') ? 'is-invalid' : '' }}" id="pib" name="pib" placeholder="[{{ __('VAT number') }}]" value="{{ old('pib', $client->pib ?? '') }}" required />
        <div class="invalid-feedback">
            @if ($errors->has('pib'))
               @foreach ($errors->get('pib') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ __('This field is mandatory!') }}
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="mb" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Registration number') }}</label>
    <div class="col-lg-5">
        <input type="number" step="1" class="form-control form-control-sm {{ $errors->has('mb') ? 'is-invalid' : '' }}" id="mb" name="mb" placeholder="[{{ __('Registration number') }}]" value="{{ old('mb', $client->mb ?? '') }}" />
        <div class="invalid-feedback">
            @if ($errors->has('mb'))
               @foreach ($errors->get('mb') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ __('This field is mandatory!') }}
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-lg-2 col-form-label col-form-label-sm">Email</label>
    <div class="col-lg-5">
        <input type="text" class="form-control form-control-sm {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" name="email" placeholder="[email]" value="{{ old('email', $client->email ?? '') }}" required />
        <div class="invalid-feedback">
            @if ($errors->has('email'))
                @foreach ($errors->get('email') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ __('This field is mandatory!') }}
            @endif
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="tel" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Telephone') }}</label>
    <div class="col-lg-5">
        <input type="text" class="form-control form-control-sm" id="tel" name="tel" placeholder="[tel]" value="{{ old('tel', $client->tel ?? '') }}" />
    </div>
</div>

<div class="form-group row">
    <label for="notes" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Notes') }}</label>
    <div class="col-lg-5">
        <textarea type="text" class="form-control form-control-sm" id="notes" name="notes" rows="3" placeholder="[{{ __('Notes') }}]">{{ old('notes', $client->notes ?? '') }}</textarea>
    </div>
</div>