@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form action="{{ route('clients.store') }}" method="POST" class="row px-3 needs-validation" novalidate>

            @csrf

            <div class="col">

                <div class="row">
                    <div class="col">
                        
                        <h1 class="mt-5 mb-2">{{ __('Address book') }}</h1>

                        <h2>{{ __('Add new client') }}</h2>
                        <hr>

                        @include('partials.alerts')

                        @include('clients.form-fields')

                    </div>
                </div>

            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-primary my-1 w-100">{{ __('Save') }}</button>
                    <a href="{{ route('clients.index') }}" class="btn btn-secondary my-1 w-100">{{ __('Cancel') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>

            </div>

        </form>

    </div>

</div>

@endsection