<div>
    <div class="form-group row">
        <label for="name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Title') }}</label>
        <div class="col-lg-5">
            <input type="text" class="form-control form-control-sm {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="[{{ __('Title') }}]" value="{{ old('name', $machine->name ?? '') }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('name'))
                    @foreach ($errors->get('name') as $message)
                        {{ $message }}
                    @endforeach
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>                            
    
    <div class="form-group row">
        <label for="description" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Description') }}</label>
        <div class="col-lg-5">
            <textarea type="text" class="form-control form-control-sm" id="description" name="description" rows="3" placeholder="[{{ __('Description') }}]">{{ old('description', $machine->description ?? '') }}</textarea>
        </div>
    </div>

    <fieldset class="form-group">
        <div class="row">
            <label for="work_and_turn" class="col-form-label col-form-label-sm col-lg-2">{{ __('Work and turn (capable)') }}</label>
            <div class="col-lg-5">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="work_and_turn" id="wt_yes" value="1" {{ old('work_and_turn', $machine->work_and_turn ?? false) ? 'checked' : '' }} />
                    <label class="form-check-label col-form-label-sm" for="wt_yes">{{ __('Yes') }}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="work_and_turn" id="wt_no" value="0" {{ old('work_and_turn', $machine->work_and_turn ?? false) ? 'checked' : '' }} />
                    <label class="form-check-label col-form-label-sm" for="wt_no">{{ __('No') }}</label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="cn" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Number of colours in one pass') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('colour_no') ? 'is-invalid' : '' }}" id="cn" name="colour_no" placeholder="[{{ __('Number of colours in one pass') }}]" value="{{ old('colour_no', $machine->colour_no ?? '') }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('colour_no'))
                   @foreach ($errors->get('colour_no') as $message)
                        {{ $message }}
                    @endforeach
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="mk" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Makulatur per colour') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('makulatur') ? 'is-invalid' : '' }}" id="mk" name="makulatur" placeholder="[{{ __('Makulatur per colour') }}]" value="{{ old('makulatur', $machine->makulatur ?? '') }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('makulatur'))
                   @foreach ($errors->get('mmakulaturk') as $message)
                        {{ $message }}
                    @endforeach
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="pp" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Plate price') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('pp') ? 'is-invalid' : '' }}" id="pp" name="plate_price" placeholder="[EUR]" value="{{ old('plate_price', $machine->plate_price ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('plate_price'))
                   @foreach ($errors->get('plate_price') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="sp" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Starting price (price up to 1000 sheets, per colour)') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('start_price') ? 'is-invalid' : '' }}" id="sp" name="start_price" placeholder="[EUR]" value="{{ old('start_price', $machine->start_price ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('start_price'))
                   @foreach ($errors->get('start_price') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="ppt" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Price per 1000 seets (after first 1000, per colour)') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('price_per_tausand') ? 'is-invalid' : '' }}" id="ppt" name="price_per_tausand" placeholder="[EUR]" value="{{ old('price_per_tausand', $machine->price_per_tausand ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('price_per_tausand'))
                   @foreach ($errors->get('price_per_tausand') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="sph" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Starting price when calculating per hour (preparing)') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('start_price_per_hour') ? 'is-invalid' : '' }}" id="sph" name="start_price_per_hour" placeholder="[EUR]" value="{{ old('start_price_per_hour', $machine->start_price_per_hour ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('start_price_per_hour'))
                   @foreach ($errors->get('start_price_per_hour') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="pph" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Price per hour') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('price_per_hour') ? 'is-invalid' : '' }}" id="pph" name="price_per_hour" placeholder="[EUR]" value="{{ old('price_per_hour', $machine->price_per_hour ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('price_per_hour'))
                   @foreach ($errors->get('price_per_hour') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="st" class="col-lg-2 col-form-label col-form-label-sm">{{ __('How long it take to start printing') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('start_time') ? 'is-invalid' : '' }}" id="st" name="start_time" placeholder="[min.]" value="{{ old('start_time', $machine->start_time ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('start_time'))
                   @foreach ($errors->get('start_time') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="stsph" class="col-lg-2 col-form-label col-form-label-sm">{{ __('How many seets per hour') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm {{ $errors->has('sheets_per_hour') ? 'is-invalid' : '' }}" id="stsph" name="sheets_per_hour" placeholder="[kom]" value="{{ old('sheets_per_hour', $machine->sheets_per_hour ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('sheets_per_hour'))
                   @foreach ($errors->get('sheets_per_hour') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="sheet_ids" class="col-lg-2 col-form-label col-form-label-sm">
            {{ __('Press sheets it can print on') }}
        </label>
        <div class="col-lg-5">
            <select class="selectpicker col-12 px-0" id="area_ids" name="press_sheets[]" multiple data-none-selected-text="({{ __('leave blank for all') }})">

                @forelse($press_sheets as $press_sheet)
                    <option value="{{ $press_sheet['id'] }}"
                            {{ in_array($press_sheet->id, old('sheets', ($machine ?? []) ? ($machine->pressSheets->pluck('id')->toArray()) : [])) ? 'selected' : '' }}>
                        {{ $press_sheet->name }}
                    </option>
                @empty
                    {{ __('No entries') }}
                @endforelse
            </select>
        </div>
    </div>
</div>