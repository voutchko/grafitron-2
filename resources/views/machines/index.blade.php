@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form class="row px-3">

            @csrf

            <div class="col">

            	<div class="row">
            		<div class="col">
            			
            			<h1 class="mt-5 mb-2">{{ __('Machine') }}</h1>

            			<h2>{{ __('Here you can define presses and printers') }}</h2>
		            	<hr>

		            	@include('partials.alerts')

		            	<div>
					        <table class="ck" data-sort="table">
					            <thead>
					            <tr>
					                <th class="header">{{ __('Title') }}</th>
					                <th class="header">{{ __('Description') }}</th>
					                <th class="header">{{ __('Edit') }}</th>
					            </tr>
					            </thead>
					            <tbody>
					            	@if($machines->count())
					            		@foreach ($machines as $machine)
							                <tr class="clickable-row" data-href="{{ route('machines.edit', $machine->id) }}">
							                    <td>{{ $machine->name }}</td>
							                    <td>{{ $machine->description }}</td>
							                    <td><i class="fa fa-caret-right"></i></td>
							                </tr>
							            @endforeach
						              @else
						              	<tr class="clickable-row" data-href="">
						                    <td colspan="3">{{ __('No entries') }}</td>
						                </tr>
						              @endif
					            </tbody>
					        </table>
					    </div>

            		</div>
            	</div>
            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <a class="btn btn-primary my-1 w-100" href="{{ route('machines.create') }}">{{ __('Add new press') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>

@endsection