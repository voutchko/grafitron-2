@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form class="row px-3">

            @csrf

            <div class="col">

            	<div class="row">
            		<div class="col">
            			
            			<h1 class="mt-5 mb-2">{{ __('Base sheets') }}</h1>

            			<h2>{{ __('Here you can define the sheet dimensions in wich the paper is being bought.') }}</h2>
		            	<hr>

		            	@include('partials.alerts')

		            	<div>
					        <table class="ck" data-sort="table">
					            <thead>
					            <tr>
					                <th class="header">{{ __('Sheet') }}</th>
					                <th class="header">{{ __('Dimensions') }}</th>
					                <th class="header">{{ __('Edit') }}</th>
					            </tr>
					            </thead>
					            <tbody>
					            	@if($base_sheets->count())
					            		@foreach ($base_sheets as $base_sheet)
							                <tr class="clickable-row" data-href="{{ route('base-sheets.edit', $base_sheet->id) }}">
							                    <td>{{ $base_sheet->name }}</td>
							                    <td>{{ @formatNumber($base_sheet->x, 1) }} x {{ @formatNumber($base_sheet->y, 1) }} mm</td>
							                    <td><i class="fa fa-caret-right"></i></td>
							                </tr>
							            @endforeach
						              @else
						              	<tr class="clickable-row" data-href="">
						                    <td colspan="3">{{ __('There is no defined sheets') }}</td>
						                </tr>
						              @endif
					            </tbody>
					        </table>
					    </div>

            		</div>
            	</div>

            </div>


            <div id="sidebar" class="col-auto">
                
                <div class="text-center mt-5">
                    <a class="btn btn-primary my-1 w-100" href="{{ route('base-sheets.create') }}">{{ __('Add new sheet') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>


@endsection