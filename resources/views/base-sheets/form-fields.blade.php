<div>
	<div class="form-group form-row">
        <label for="name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Sheet title') }}</label>
        <div class="col-lg-5">
            <input type="text" class="form-control form-control-sm {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="[format]" value="{{ old('name') ?? $baseSheet->name ?? '' }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('name'))
                    {{ __('This field is already in the database!') }}
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>
    <div class="form-group form-row">
        <label for="x" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Dimensions') }}</label>
        <div class="col-2">
            <input type="number" step="0.5" class="form-control form-control-sm" id="x" name="x" placeholder="x [mm]" value="{{ old('x') ?? $baseSheet->x ?? '' }}" required />
            <div class="invalid-feedback">{{ __('This field is mandatory!') }}</div>
        </div>
        <div class="col-1 col-form-label col-form-label-sm text-center"> x </div>
        <div class="col-2">
            <input type="number" step="0.5" class="form-control form-control-sm" id="y" name="y" placeholder="y [mm]" value="{{ old('y') ?? $baseSheet->y ?? '' }}" required />
            <div class="invalid-feedback">{{ __('This field is mandatory!') }}</div>
        </div>
    </div>
</div>