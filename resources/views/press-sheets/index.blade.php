@extends('layouts.app')

@section('content')


<div class="container-fluid">
    <div class="">

        <form action="{{ route('press-sheets.filter') }}" method="post" class="row px-3">

            @csrf

            <div class="col">

            	<div class="row">
            		<div class="col">
            			
            			<h1 class="mt-5 mb-2">{{ __('Press sheets') }}</h1>

            			<h2>{{ __('Here you can define press sheets') }}</h2>
		            	<hr>

                        @include('partials.alerts')

		            	<div class="form-group row">
                            <label for="base_sheet" class="col-lg-1 col-form-label">{{ __('Base sheet') }}</label>
                            <div class="col-lg-4">
                                <select class="form-control" id="base_sheet" name="base_sheet" placeholder="[format]">
                                	@foreach($base_sheets as $base_sheet)
                                        <option value="{{ $base_sheet->id }}"
                                                {{ $base_sheet->id === $base_sheet_selected->id ? 'selected' : ''  }}>
                                            {{ $base_sheet->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4"><button type="submit" class="btn btn-primary">{{ __('List') }}</button></div>
                        </div>
                        
                        <hr>

		            	<div>
					        <table class="ck" data-sort="table">
					            <thead>
					            <tr>
					                <th class="header">{{ __('Press sheet') }}</th>
					                <th class="header">{{ __('How many press sheets you get from the base sheet') }}</th>
					                <th class="header">{{ __('Dimensions') }}</th>
					                <th class="header">{{ __('Edit') }}</th>
					            </tr>
					            </thead>
					            <tbody>
					            	@forelse ($press_sheets as $press_sheet)
				            		    <tr class="clickable-row" data-href="{{ route('press-sheets.edit', $press_sheet->id) }}">
						                    <td>{{ $press_sheet->name }}</td>
						                    <td>{{ $press_sheet->how_many }}</td>
						                    <td>{{ @formatNumber($press_sheet->x, 1) }} x {{ @formatNumber($press_sheet->y, 1) }} mm</td>
						                    <td><i class="fa fa-caret-right"></i></td>
						                </tr>
						            @empty
					              	<tr class="clickable-row" data-href="">
					                    <td colspan="4">{{ __('There is no defined sheets') }}</td>
					                </tr>
						              @endforelse
					            </tbody>
					        </table>
					    </div>

            		</div>
            	</div>

            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <a class="btn btn-primary my-1 w-100" href="{{ route('press-sheets.create', $base_sheet_selected->id) }}">{{ __('Add new sheet') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>

@endsection