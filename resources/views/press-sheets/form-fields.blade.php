<div>
    <div class="form-group row">
        <label for="base_sheet_id" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Base sheet') }}</label>
        <div class="col-lg-5">
            <select class="form-control form-control-sm" id="base_sheet_id" name="base_sheet_id" placeholder="[format]" required>
                @foreach($base_sheets as $base_sheet)
                    <option value="{{ $base_sheet->id }}"
                            {{ old( 'base_sheet_id', ($press_sheet->base_sheet_id ?? $base_sheet_id ?? '') ) === $base_sheet->id ? 'selected' : ''  }}>
                        {{ $base_sheet->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="how_many" class="col-lg-2 col-form-label col-form-label-sm">{{ __('How many press sheets you get from the base sheet') }}</label>
        <div class="col-lg-5">
            <input type="number" class="form-control form-control-sm" id="how_many" name="how_many" placeholder="[kom]" value="{{ old('how_many', $press_sheet->how_many ?? '') }}" required />
            <div class="invalid-feedback">{{ __('This field is mandatory!') }}</div>
        </div>
    </div>
     <div class="form-group row">
        <label for="name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Sheet title') }}</label>
        <div class="col-lg-5">
            <input type="text" class="form-control form-control-sm {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="[format]" value="{{ old('name', $press_sheet->name ?? '') }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('name'))
                    {{ __('This field is already in the database!') }}
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="x" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Dimensions') }}</label>
        <div class="col-2">
            <input type="number" step="0.5" class="form-control form-control-sm" id="x" name="x" placeholder="x [mm]" value="{{ old('x', $press_sheet->x ?? '') }}" required />
            <div class="invalid-feedback">{{ __('This field is mandatory!') }}</div>
        </div>
        <div class="col-1 col-form-label col-form-label-sm text-center"> x </div>
        <div class="col-2">
            <input type="number" step="0.5" class="form-control form-control-sm" id="y" name="y" placeholder="y [mm]" value="{{ old('y', $press_sheet->y ?? '') }}" required />
            <div class="invalid-feedback">{{ __('This field is mandatory!') }}</div>
        </div>
    </div>
</div>