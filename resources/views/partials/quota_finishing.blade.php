<div id="plast-{{ $finishing->id }}" class="tabak border border-secondary rounded mt-3 p-3">

    <div id="filters-{{ $finishing->id }}" class="row">
        <div class="col-lg-3">
            <h1>{{ $finishing->name }}</h1>
        </div>
        <div class="col-lg-3 border-left">
            Opis: <strong>{{ $finishing->description }}</strong><br>
            Tiraž: <strong>{{ $finishing->run }}</strong>
        </div>
        <div class="col-lg-3 border-left">
            <a href="{{ route('quota-finishings.edit', [$quota->id, $finishing->id] ) }}" class="btn btn-primary my-1 mr-3">Izmeni</a>
        </div>
        <div class="col-lg-2 border-left">
            CENA: <strong>
                @isset($calculation['finishing'][$finishing->id]) 
                    {{ $calculation['finishing'][$finishing->id] . ' €'}} 
                @endisset
            </strong><br>
        </div>
        <div class="col-lg-1 text-right">
            <h2></h2>
        </div>
    </div>

</div>