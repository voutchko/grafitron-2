<div id="plast-{{ $plast_no }}" class="tabak border border-secondary rounded mt-3 p-3">

    <div id="filters-{{ $plast_no }}" class="row">
        <div class="col-lg-3">
            <h1>Plastifikacija {{ $plast_no }}</h1>
        </div>
        <div class="col-lg-3 border-left">
            Opis: <strong>{{ $plastic->description }}</strong><br>
            Format: <strong>{{ $plastic->sheet }}</strong><br>
            Obostrano: <strong>{{ $plastic->both_sides ? 'Da' : 'Ne' }}</strong><br>
            Tiraž: <strong>{{ $plastic->run }}</strong>
        </div>
        <div class="col-lg-3 border-left">
            <a href="{{ route('quota-plastic-coatings.edit', [$quota->id, $plastic->id] ) }}" class="btn btn-primary my-1 mr-3">Izmeni</a>
        </div>
        <div class="col-lg-2 border-left">
            CENA: <strong>
                @isset($calculation['plastics'][$plast_no]) 
                    {{ $calculation['plastics'][$plast_no] . ' €'}} 
                @endisset
            </strong><br>
        </div>
        <div class="col-lg-1 text-right">
            <h2></h2>
        </div>
    </div>

</div>