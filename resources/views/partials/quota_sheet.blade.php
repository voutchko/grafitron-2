<div id="tabak-{{ $tab_no }}" class="tabak border border-secondary rounded mt-3 p-3 tabak {{ $tab_no !== 'new' ? 'open' : '' }}">

    <div id="filters-{{ $tab_no }}" class="filters row {{ $tab_no !== 'new' ? 'open' : '' }}"  data-tabak="{{ $tab_no }}">
        <div class="col-lg-3">
            <h1>Tabak {{ $red_no }}</h1>
        </div>
        <div class="col-lg-3 border-left">
            Mašina: <strong>{{ $calculation['sheets'][$tab_no]['masina'] ?? ''}}</strong><br>
            Papir: <strong>{{ $calculation['sheets'][$tab_no]['papir'] ?? ''}}</strong><br>
            Broj boja: <strong>{{ $calculation['sheets'][$tab_no]['broj_boja'] ?? ''}}</strong><br>
            Tiraž: <strong>{{ $calculation['sheets'][$tab_no]['tiraz'] ?? ''}}</strong>
        </div>
        <div class="col-lg-3 border-left">
            tabaka: <strong>{{ $calculation['sheets'][$tab_no]['tab'] ?? ''}}</strong><br>
            kg: <strong>{{ $calculation['sheets'][$tab_no]['kg'] ?? ''}}</strong><br>
            vreme: <strong>{{ $calculation['sheets'][$tab_no]['vreme'] ?? ''}}</strong><br>
            cena po satu: <strong>{{ $calculation['sheets'][$tab_no]['cena_po_satu'] ?? ''}} €</strong>
        </div>
        <div class="col-lg-2 border-left">
            CENA: <strong>{{ $calculation['sheets'][$tab_no]['cena'] ?? ''}}</strong><br>
        </div>
        <div class="col-lg-1 text-right">
            <h2></h2>
        </div>
    </div>

    <hr>

    

    <div class="row">

        <div class="col-lg-4">

            <fieldset class="form-group">
                <div class="row">
                    <label for="price_per_hour{{ '_' . $tab_no }}" class="col-form-label col-form-label-sm col-lg-6"><strong>Cena po satu</strong></label>
                    <div class="col-lg-5">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input price_per_hour_yes" type="radio" name="price_per_hour[{{ $tab_no }}]" id="price_per_hour_yes{{ '_' . $tab_no }}" value="1" {{ old('price_per_hour['.$tab_no.']', $quota_sheet->price_per_hour ?? '') === 1 ? 'checked' : ''  }} />
                            <label class="form-check-label col-form-label-sm" for="price_per_hour_yes{{ '_' . $tab_no }}">Da</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input price_per_hour_no" type="radio" name="price_per_hour[{{ $tab_no }}]" id="price_per_hour_no{{ '_' . $tab_no }}" value="0" {{ old('price_per_hour['.$tab_no.']', $quota_sheet->price_per_hour ?? '') === 0 ? 'checked' : ''  }}  />
                            <label class="form-check-label col-form-label-sm" for="price_per_hour_no{{ '_' . $tab_no }}">Ne</label>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="form-group row">
                <label for="how_many{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Koliko ovakvih tabaka</strong></label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="how_many{{ '_' . $tab_no }}" name="how_many[{{ $tab_no }}]" placeholder="[kom]" step="1" value="{{ old('how_many.'.$tab_no, $quota_sheet->how_many  ?? '') }}" required />
                </div>
            </div>

            <div class="form-group row">
                <label for="run{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Tiraž proizvoda</strong></label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="run{{ '_' . $tab_no }}" name="run[{{ $tab_no }}]" placeholder="[kom]" value="{{ old('run.'.$tab_no, $quota->how_many  ?? '') }}" readonly />
                </div>
            </div>

            <div class="form-group row">
                <label for="hm{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Koliko ih staje na tabak</strong></label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="hm{{ '_' . $tab_no }}" name="hm[{{ $tab_no }}]" placeholder="[kom]" value="{{ old('hm.'.$tab_no, $quota_sheet->hm  ?? '') }}" required />
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label for="ps{{ '_' . $tab_no }}" class="col-lg-3 col-form-label col-form-label-sm">Štampani tabak</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control form-control-sm" id="ps{{ '_' . $tab_no }}" name="ps[{{ $tab_no }}]" placeholder="[format]" value="{{ old('ps.'.$tab_no, $quota_sheet->ps  ?? '') }}"/>
                </div>
                <div class="col-lg-3">
                    <select class="form-control form-control-sm select_sheet" id="select_sheet{{ '_' . $tab_no }}" name="select_sheet{{ '_' . $tab_no }}">
                        <option value="">Nađi...</option>
                        @foreach($sheets as $sheet)
                            <option value="{{ $sheet->id }}">{{ $sheet->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group form-row">
                <label for="psx{{ '_' . $tab_no }}" class="col-lg-5 col-form-label col-form-label-sm">Dimenzija tabaka</label>
                        <div class="col-3">
                            <input type="number" class="form-control form-control-sm" id="psx{{ '_' . $tab_no }}" name="psx[{{ $tab_no }}]" placeholder="x [mm]" value="{{ old('psx.'.$tab_no, $quota_sheet->psx  ?? '') }}"/>
                        </div>
                        <div class="col-3">
                            <input type="number" class="form-control form-control-sm" id="psy{{ '_' . $tab_no }}" name="psy[{{ $tab_no }}]" placeholder="y [mm]" value="{{ old('psy.'.$tab_no, $quota_sheet->psy  ?? '') }}"/>
                        </div>
            </div>

            <div class="form-group row">
                <label for="hhm{{ '_' . $tab_no }}" class="col-lg-7 col-form-label col-form-label-sm"><strong>Koliko stampanih u kupovnom</strong></label>
                <div class="col-lg-4">
                    <input type="number" class="form-control form-control-sm" id="hhm{{ '_' . $tab_no }}" name="hhm[{{ $tab_no }}]" placeholder="[kom]" value="{{ old('hhm.'.$tab_no, $quota_sheet->hhm  ?? '') }}" required/>
                </div>
            </div>

            <hr>

            <div class="form-group form-row">
                <label for="ca{{ '_' . $tab_no }}" class="col-lg-4 col-form-label col-form-label-sm"><strong>Broj boja</strong></label>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" id="ca{{ '_' . $tab_no }}" name="ca[{{ $tab_no }}]" placeholder="A [kom]" value="{{ old('ca.'.$tab_no, $quota_sheet->ca  ?? '') }}"/>
                </div>
                <div class="col-1 col-form-label col-form-label-sm"> / </div>
                <div class="col-3">
                    <input type="number" class="form-control form-control-sm" id="cb{{ '_' . $tab_no }}" name="cb[{{ $tab_no }}]" placeholder="B [kom]" value="{{ old('cb.'.$tab_no, $quota_sheet->cb  ?? '') }}"/>
                </div>
            </div>

            <fieldset class="form-group">
                <div class="row">
                    <label for="wt{{ '_' . $tab_no }}" class="col-form-label col-form-label-sm col-lg-6"><strong>Okretanje</strong></label>
                    <div class="col-lg-5">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="wt[{{ $tab_no }}]" id="wt_yes{{ '_' . $tab_no }}" value="1" {{ old('wt['.$tab_no.']', $quota_sheet->wt ?? '') === 1 ? 'checked' : ''  }} />
                            <label class="form-check-label col-form-label-sm" for="wt_yes{{ '_' . $tab_no }}">Da</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="wt[{{ $tab_no }}]" id="wt_no{{ '_' . $tab_no }}" value="0" {{ old('wt['.$tab_no.']', $quota_sheet->wt ?? 0) === 0 ? 'checked' : ''  }} />
                            <label class="form-check-label col-form-label-sm" for="wt_no{{ '_' . $tab_no }}">Ne</label>
                        </div>
                    </div>
                </div>
            </fieldset>

        </div>

        <div class="col-lg-4">

            <div class="form-group row">
                <label for="mk{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Makulatura po boji</strong></label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="mk{{ '_' . $tab_no }}" name="mk[{{ $tab_no }}]" placeholder="[kom]" value="{{ old('mk.'.$tab_no, $quota_sheet->mk ?? '') }}"/>
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label for="ppr{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Cena ploče</strong></label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="ppr{{ '_' . $tab_no }}" name="ppr[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('ppr.'.$tab_no, $quota_sheet->ppr ?? '') }}"/>
                </div>
            </div>

            <fieldset id="per_sheet{{ '_' . $tab_no }}" {{ old('price_per_hour['.$tab_no.']', $quota_sheet->price_per_hour ?? '') === 1 ? 'disabled' : ''  }}>

                <div class="form-group row">
                    <label for="spr{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Cena starta</strong></label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control form-control-sm" id="spr{{ '_' . $tab_no }}" name="spr[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('spr.'.$tab_no, $quota_sheet->spr ?? '') }}"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ppt{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Cena nastavka</strong></label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control form-control-sm" id="ppt{{ '_' . $tab_no }}" name="ppt[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('ppt.'.$tab_no, $quota_sheet->ppt ?? '') }}"/>
                    </div>
                </div>

            </fieldset>

            <hr>

            <fieldset id="per_hour{{ '_' . $tab_no }}" {{ old('price_per_hour['.$tab_no.']', $quota_sheet->price_per_hour ?? '') === 0 ? 'disabled' : ''  }}>

                <div class="form-group row">
                    <label for="sph{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm">Cena starta po satu</label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control form-control-sm" id="sph{{ '_' . $tab_no }}" name="sph[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('sph.'.$tab_no, $quota_sheet->sph ?? '') }}" />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="pph{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm">Cena po satu</label>
                    <div class="col-lg-5">
                        <input type="number" class="form-control form-control-sm" id="pph{{ '_' . $tab_no }}" name="pph[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('pph.'.$tab_no, $quota_sheet->pph ?? '') }}"/>
                    </div>
                </div>

            </fieldset>

            <hr>

            <div class="form-group row">
                <label for="stsph{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm">Tabaka na sat</label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="stsph{{ '_' . $tab_no }}" name="stsph[{{ $tab_no }}]" placeholder="[kom]" value="{{ old('stsph.'.$tab_no, $quota_sheet->stsph ?? '') }}"/>
                </div>
            </div>

            <div class="form-group row">
                <label for="st{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm">Ubacivanje ploča</label>
                <div class="col-lg-5">
                    <input type="number" class="form-control form-control-sm" id="st{{ '_' . $tab_no }}" name="st[{{ $tab_no }}]" placeholder="[min]" value="{{ old('st.'.$tab_no, $quota_sheet->st ?? '') }}"/>
                </div>
            </div>

        </div>

        <div class="col-lg-4">

            <div class="form-group row">
                <label for="machine_name{{ '_' . $tab_no }}" class="col-lg-3 col-form-label col-form-label-sm">Mašina</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control form-control-sm" id="machine_name{{ '_' . $tab_no }}" name="machine_name[{{ $tab_no }}]" placeholder="Mašina" value="{{ old('machine_name.'.$tab_no, $quota_sheet->machine_name ?? '') }}"/>
                </div>
                <div class="col-lg-3">
                    <select class="form-control form-control-sm select_machine" id="select_machine{{ '_' . $tab_no }}" name="select_machine{{ '_' . $tab_no }}">
                        <option value="">Nađi...</option>
                        @foreach($machines as $machine)
                            <option value="{{ $machine->id }}">{{ $machine->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="machine_description{{ '_' . $tab_no }}" class="col-lg-3 col-form-label col-form-label-sm">Opis mašine</label>
                <div class="col-lg-8">
                    <textarea class="form-control form-control-sm" id="machine_description{{ '_' . $tab_no }}" name="machine_description[{{ $tab_no }}]" rows="2" placeholder="Opis mašine">{{ old('machine_description.'.$tab_no, $quota_sheet->machine_description ?? '') }}</textarea>
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label for="pp{{ '_' . $tab_no }}" class="col-lg-3 col-form-label col-form-label-sm">Papir</label>
                <div class="col-lg-5">
                    <input type="text" class="form-control form-control-sm" id="pp{{ '_' . $tab_no }}" name="pp[{{ $tab_no }}]" placeholder="Papir" value="{{ old('pp.'.$tab_no, $quota_sheet->pp ?? '') }}"/>
                </div>
                <div class="col-lg-3">
                    <select class="form-control form-control-sm select_paper" id="select_paper{{ '_' . $tab_no }}" name="select_paper{{ '_' . $tab_no }}">
                        <option value="">Nađi...</option>
                        @foreach($papers as $paper)
                            <option value="{{ $paper->id }}">{{ $paper->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="ppd{{ '_' . $tab_no }}" class="col-lg-3 col-form-label col-form-label-sm">Papir - opis</label>
                <div class="col-lg-8">
                    <textarea class="form-control form-control-sm" id="ppd{{ '_' . $tab_no }}" name="ppd[{{ $tab_no }}]" rows="2" placeholder="Papir - opis">{{ old('ppd.'.$tab_no, $quota_sheet->ppd ?? '') }}</textarea>
                </div>
            </div>

            <div class="form-group form-row">
                <label for="ppsx{{ '_' . $tab_no }}" class="col-lg-2 col-form-label col-form-label-sm"><strong>Dimenzije</strong></label>
                <div class="col-2">
                    <input type="text" class="form-control form-control-sm" id="pps{{ '_' . $tab_no }}" name="pps[{{ $tab_no }}]" placeholder="[B1]" value="{{ old('pps.'.$tab_no, $quota_sheet->pps ?? '') }}"/>
                </div>
                <div class="col-3 offset-1">
                    <input type="text" class="form-control form-control-sm" id="ppsx{{ '_' . $tab_no }}" name="ppsx[{{ $tab_no }}]" placeholder="x [mm]" value="{{ old('ppsx.'.$tab_no, $quota_sheet->ppsx ?? '') }}"/>
                </div>
                <div class="col-3">
                    <input type="text" class="form-control form-control-sm" id="ppsy{{ '_' . $tab_no }}" name="ppsy[{{ $tab_no }}]" placeholder="y [mm]" value="{{ old('ppsy.'.$tab_no, $quota_sheet->ppsy ?? '') }}"/>
                </div>
            </div>

            <fieldset class="form-group">
                <div class="row">
                    <label for="price_per_sheet{{ '_' . $tab_no }}" class="col-form-label col-form-label-sm col-lg-6">Cena papira po tabaku</label>
                    <div class="col-lg-5">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input price_per_sheet_yes" type="radio" name="price_per_sheet[{{ $tab_no }}]" id="price_per_sheet_yes{{ '_' . $tab_no }}" value="1" {{ old('price_per_sheet['.$tab_no.']', $quota_sheet->price_per_sheet ?? '') === 1 ? 'checked' : ''  }} />
                            <label class="form-check-label col-form-label-sm" for="price_per_sheet_yes{{ '_' . $tab_no }}">Da</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input price_per_sheet_no" type="radio" name="price_per_sheet[{{ $tab_no }}]" id="price_per_sheet_no{{ '_' . $tab_no }}" value="0"  {{ old('price_per_sheet['.$tab_no.']', $quota_sheet->price_per_sheet ?? 0) === 0 ? 'checked' : ''  }} />
                            <label class="form-check-label col-form-label-sm" for="price_per_sheet_no{{ '_' . $tab_no }}">Ne</label>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="form-group row">
                <label for="pw{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Spec. težina papira</strong></label>
                <div class="col-lg-5">
                    <input type="text" class="form-control form-control-sm" id="pw{{ '_' . $tab_no }}" name="pw[{{ $tab_no }}]" placeholder="[kg / m3]" value="{{ old('pw.'.$tab_no, $quota_sheet->pw ?? '') }}"/>
                </div>
            </div>

            <fieldset id="per_kg{{ '_' . $tab_no }}">
                <div class="form-group row">
                    <label for="prk{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm"><strong>Cena po kg</strong></label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control form-control-sm" id="prk{{ '_' . $tab_no }}" name="prk[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('prk.'.$tab_no, $quota_sheet->prk ?? '') }}"/>
                    </div>
                </div>
            </fieldset>

            <fieldset id="per_sheet_p{{ '_' . $tab_no }}" disabled>
                <div class="form-group row">
                    <label for="prps{{ '_' . $tab_no }}" class="col-lg-6 col-form-label col-form-label-sm">Cena po tabaku</label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control form-control-sm" id="prps{{ '_' . $tab_no }}" name="prps[{{ $tab_no }}]" placeholder="[eur]" value="{{ old('prps.'.$tab_no, $quota_sheet->prps ?? '') }}"/>
                    </div>
                </div>
            </fieldset>
        
        </div>

        <div class="col-11">

            <button type="submit" class="btn btn-primary my-1 mr-3">Snimi / Izračunaj</button>
            @if($tab_no != 'new')
             <a href="#" class="btn btn-primary my-1 mr-3" onclick="event.preventDefault();document.getElementById('copy_sheet').value={{ $quota_sheet->id ?? 'new' }};document.getElementById('quota_form').submit()
                    ">Iskopiraj</a>

               <a href="#" class="btn btn-danger my-1" onclick="if(confirm('Siguran?')){
                        event.preventDefault();document.getElementById('delete_sheet').value={{ $quota_sheet->id }};document.getElementById('quota_form').submit()
                    }">Obriši</a>
            @endif

        </div>

    </div>

</div>