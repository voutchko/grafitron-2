@if(session()->has('message'))
    <div class="alert alert-dismissable alert-{{ session()->get('status') }}">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('message') }}
    </div>
@endif

@if(isset($errors) && count($errors) > 0)
    <div class="alert alert-dismissable alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach (array_unique($errors->all()) as $error)
                <li class="list-unstyled">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
