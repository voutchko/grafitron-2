<!doctype html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Grafitron 2.0') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- non-laravel styles -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <meta name="Author" content="voutchko@gmail.com"/>
    <meta name="Copyright" content="2019 Voutchko"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/assets/fontawesome/css/font-awesome.css"/>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-select.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/assets/fontawesome5/brands.css"/>
    <link rel="stylesheet" href="/assets/fontawesome5/solid.css"/>
    <link rel="stylesheet" href="/assets/fontawesome5/fontawesome.min.css"/>
    <link rel="stylesheet" href="/assets/css/image-picker.css">
    <link rel="stylesheet" href="/assets/css/slick.css">
    <link rel="stylesheet" href="/assets/css/style.css">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Grafitron 2.0') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>

<section class="body" id="navigacija">
    <div class="container-fluid">
        <div class="row">

            @yield('content')

        </div>
    </div>
</section>

<a id="scroll_up">
    <i class="flaticon-up-arrow-angle"></i>
</a>

<!-- jquery  -->
<script src="/assets/js/jquery-3.4.1.js"></script>
<!-- Bootstrap js  -->
<script src="/assets/bootstrap/js/bootstrap.bundle.js"></script>

<!-- Bootstrap selectpicker -->
<script src="/assets/bootstrap/js/bootstrap-select.js"></script>
<!-- Bootstrap datepicker -->
<script src="/assets/bootstrap/js/bootstrap-datepicker.js"></script>

<!-- polyfill -->
<script src="/assets/js/number-polyfill.min.js"></script>

<!-- Font awsome -->
<script src="/assets/fontawesome5/fontawesome.min.js"></script>

<!-- custom js  -->
<script src="/assets/js/image-picker.js"></script>
<script src="/assets/js/slick.js"></script>
<script src="/assets/js/custom.js"></script>
<script src="/assets/js/select-paper.js"></script>
<script src="/assets/js/select-sheet.js"></script>
<script src="/assets/js/select-machine.js"></script>
<script src="/assets/js/select-client.js"></script>
<script src="/assets/js/select-plastic.js"></script>
<script src="/assets/js/select-finishing.js"></script>

</body>
</html>