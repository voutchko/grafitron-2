@extends('layouts.app')

@section('content')
<div class="container news">

        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-lg-7">

                <div class="buttons" style="height: 85vh; align-items: center; display: flex; justify-content: center; position: relative;">
                    
                        

                    
                    <div class="row">

                        @if (Auth::user())

                            <div class="col text-center">

                                @include('partials.alerts')

                                @if(1)
                                
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="{{ route('base-sheets.index') }}">
                                        <i class="fas fa-layer-group fa-2x mb-3"></i>
                                        <h4>{{ __('Base sheets') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="{{ route('press-sheets.index') }}">
                                        <i class="fas fa-file-powerpoint fa-2x mb-3"></i>
                                        <h4>{{ __('Press sheets') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="{{ route('papers.index') }}">
                                        <i class="fas fa-toilet-paper fa-2x mb-4"></i>
                                        <h4>{{ __('Paper') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="{{ route('machines.index') }}">
                                        <i class="fas fa-print fa-2x mb-4"></i>
                                        <h4>{{ __('Presses') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="">
                                        <i class="fas fa-clone fa-2x mb-4"></i>
                                        <h4>{{ __('Plastic coating') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="">
                                        <i class="fas fa-map fa-2x mb-4"></i>
                                        <h4>{{ __('Finishings') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="{{ route('clients.index') }}">
                                        <i class="fas fa-address-book fa-2x mb-4"></i>
                                        <h4>{{ __('Address book') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-turquise text-center clickable-row text-white" data-href="">
                                        <i class="fas fa-users fa-2x mb-4"></i>
                                        <h4>{{ __('Users') }}</h4>
                                    </div>
                                    <div class="news-box bg-color-magenta text-center clickable-row text-white" data-href="">
                                        <i class="fas fa-calculator fa-2x mb-3"></i>
                                        <h4>{{ __('Calculations') }}</h4>
                                    </div>

                                @endif

                                <div class="news-box bg-color-magenta text-center clickable-row text-white" data-href="">
                                    <i class="fas fa-file-invoice-dollar fa-2x mb-3"></i>
                                    <h4>{{ __('Quotas') }}</h4>
                                </div>
                                <div class="news-box bg-color-magenta text-center clickable-row text-white" data-href="">
                                    <i class="fas fa-tasks fa-2x mb-4"></i>
                                    <h4>{{ __('Work orders') }}</h4>
                                </div>
                                <div class="news-box bg-color-magenta text-center clickable-row text-white" data-href="">
                                    <i class="fas fa-tachometer-alt fa-2x mb-3"></i>
                                    <h4>{{ __('Quick Quota') }}</h4>
                                </div>

                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
