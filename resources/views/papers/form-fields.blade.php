<div>
    <div class="form-group row">
        <label for="name" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Title') }}</label>
        <div class="col-lg-5">
            <input type="text" class="form-control form-control-sm {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="[{{ __('Title') }}]" value="{{ old('name', $paper->name ?? '') }}" required />
            <div class="invalid-feedback">
                @if ($errors->has('name'))
                    @foreach ($errors->get('name') as $message)
                        {{ $message }}
                    @endforeach
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>                            
    
    <div class="form-group row">
        <label for="description" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Description') }}</label>
        <div class="col-lg-5">
            <textarea type="text" class="form-control form-control-sm" id="description" name="description" rows="3" placeholder="[{{ __('Description') }}]">{{ old('description', $paper->description ?? '') }}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for="price_per_kg" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Price per kg') }}</label>
        <div class="col-lg-5">
            <input type="number" step="0.01" class="form-control form-control-sm {{ $errors->has('price_per_kg') ? 'is-invalid' : '' }}" id="price_per_kg" name="price_per_kg" placeholder="[eur]" value="{{ old('price_per_kg', $paper->price_per_kg ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('price_per_kg'))
                   @foreach ($errors->get('price_per_kg') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="price_per_sheet" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Price per sheet') }}</label>
        <div class="col-lg-5">
            <input type="number" step="0.01" class="form-control form-control-sm {{ $errors->has('price_per_sheet') ? 'is-invalid' : '' }}" id="price_per_sheet" name="price_per_sheet" placeholder="[eur]" value="{{ old('price_per_sheet', $paper->price_per_sheet ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('price_per_sheet'))
                   @foreach ($errors->get('price_per_sheet') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="specific_weight" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Specific weights') }} ({{ __('[kg/3] - separate them by comma') }})</label>
        <div class="col-lg-5">
            <input type="text" class="form-control form-control-sm {{ $errors->has('specific_weight') ? 'is-invalid' : '' }}" id="specific_weight" name="specific_weight" placeholder="[80,100,120,135...]" value="{{ old('specific_weight', $paper->specific_weight ?? '') }}" />
            <div class="invalid-feedback">
                @if ($errors->has('specific_weight'))
                   @foreach ($errors->get('specific_weight') as $message)
                        {{ $message }}
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="base_sheet_id" class="col-lg-2 col-form-label col-form-label-sm">{{ __('Dimensions (defined in base sheet)') }}</label>
        <div class="col-lg-5">
            <select class="form-control form-control-sm col-12 px-0" id="base_sheet_id {{ $errors->has('base_sheet_id') ? 'is-invalid' : '' }}" name="base_sheet_id" required>
                <option value="">({{ __('pick a base sheet') }} - {{ __('This field is mandatory!') }})</option>
                @foreach($base_sheets as $base_sheet)
                    <option value="{{ $base_sheet['id'] }}" {{ ($base_sheet['id'] == old('base_sheet_id', $paper->base_sheet_id ?? '')) ? 'selected' : '' }}>
                        {{ $base_sheet['name'] }}
                    </option>
                @endforeach
            </select>
            <div class="invalid-feedback">
                @if ($errors->has('base_sheet_id'))
                   @foreach ($errors->get('base_sheet_id') as $message)
                        {{ $message }}
                    @endforeach
                @else
                    {{ __('This field is mandatory!') }}
                @endif
            </div>
        </div>
    </div>
</div>