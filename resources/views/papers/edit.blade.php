@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form action="{{ route('papers.update', $paper->id )}}" method="POST" class="row px-3 needs-validation" novalidate>

            @csrf
            @method('PATCH')

            <input type="number" class="d-none" name="id" value="{{ $paper->id }}" />

            <div class="col">

                <div class="row">
                    <div class="col">
                        
                        <h1 class="mt-5">{{ __('Papers') }}</h1>

                        <h2>{{ __('Edit') }}</h2>
                        <hr>

                        @include('partials.alerts')

                        @include('papers.form-fields')

                    </div>
                </div>

            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-primary my-1 w-100">{{ __('Save') }}</button>

                    <a href="#" class="btn btn-danger my-1 w-100" onclick="if(confirm('{{ __('Are you sure?') }}')){
                        event.preventDefault();document.getElementById('delete_paper').submit()
                    }">{{ __('Delete') }}</a>

                    <a href="{{ route('papers.index') }}" class="btn btn-secondary my-1 w-100">{{ __('Cancel') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>


<form id="delete_paper" action="{{ route('papers.destroy', $paper->id) }}"
      method="POST" style="display: none;" onsubmit="return confirm('{{ __('Are you sure?') }}')">
    @method('DELETE')
    @csrf
</form>

@endsection