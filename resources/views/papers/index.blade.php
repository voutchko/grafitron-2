@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="">

        <form class="row px-3">

            @csrf

            <div class="col">

            	<div class="row">
            		<div class="col">
            			
            			<h1 class="mt-5 mb-2">{{ __('Papers') }}</h1>

            			<h2>{{ __('Here you can define papers - price and dimensions') }}</h2>
		            	<hr>

		            	@include('partials.alerts')

		            	<div>
					        <table class="ck" data-sort="table">
					            <thead>
					            <tr>
					            	<th class="header header-sort-{{ $by === 'name' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'name', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Title') }}</a>
						            </th>
						            <th class="header header-sort-{{ $by === 'description' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'description', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Description') }}</a>
						            </th>
						            <th class="header">{{ __('Dimensions (base sheet)') }}</th>
						            <th class="header header-sort-{{ $by === 'w' ? ($order === 'desc' ? 'down' : 'up') : '' }}">
					                	<a href="{{ request()->fullUrlWithQuery(['by' => 'w', 'order' => $order === 'desc' ? 'asc' : 'desc']) }}">{{ __('Specific weights') }}</a>
						            </th>
					                <th class="header">{{ __('Edit') }}</th>
					            </tr>
					            </thead>
					            <tbody>
					            	@if($papers->count())
					            		@foreach ($papers as $paper)
							                <tr class="clickable-row" data-href="{{ route('papers.edit', $paper->id) }}">
							                    <td>{{ $paper->name }}</td>
							                    <td>{{ $paper->description }}</td>
							                    <td>{{ $paper->baseSheet->name }}</td>
							                    <td>{{ $paper->specific_weight }}</td>
							                    <td><i class="fa fa-caret-right"></i></td>
							                </tr>
							            @endforeach
						              @else
						              	<tr class="clickable-row" data-href="">
						                    <td colspan="5">{{ __('No entries') }}</td>
						                </tr>
						              @endif
					            </tbody>
					        </table>
					    </div>

					    <div class="pagination mt-5 w-100 align-items-center">

					    	{{ $papers->links() }}

					    </div>

            		</div>
            	</div>

            </div>


            <div id="sidebar" class="col-auto">

                <div class="text-center mt-5">
                    <a class="btn btn-primary my-1 w-100" href="{{ route('papers.create') }}">{{ __('Add new paper') }}</a>
                    
                    <br/>

                    <a class="btn btn-secondary my-1 w-100" href="/">{{ __('Back to home page') }}</a>
                </div>
                
            </div>

        </form>

    </div>

</div>

@endsection