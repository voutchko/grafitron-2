<?php

namespace App\Repositories;

use App\BaseSheet;
use Illuminate\Database\Eloquent\Collection;

class BaseSheetRepository
{
    private $model;

    public function __construct( BaseSheet $model ) {

        $this->model = $model;
    }

    public function getAll(): ?Collection {

        return $this->model->orderBy( 'name', 'asc' )->get();
    }

    public function getById( Int $id ): ?BaseSheet {

        return $this->model->find( $id );
    }

    public function store( Array $attributes ): BaseSheet {

        return $this->model->create( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->model->where( 'id', $id )
                           ->update( $attributes );
    }

    public function hasPressSheets( Int $id ): Bool {

        return $this->model->has( 'pressSheet' )->find( $id ) ? true : false;
    }

    public function delete( Int $id ): Bool {

        return $this->model->find( $id )->delete();
    }
}
