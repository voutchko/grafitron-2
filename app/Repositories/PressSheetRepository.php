<?php

namespace App\Repositories;

use App\PressSheet;
use Illuminate\Database\Eloquent\Collection;

class PressSheetRepository
{
    private $model;

    public function __construct( PressSheet $model ) {

        $this->model = $model;
    }

    public function getAll(): ?Collection {

        return $this->model->orderBy( 'name', 'asc')->get();
    }

    public function getById( Int $id ): ?PressSheet {

        return $this->model->find( $id );
    }

    public function getByBaseSheet( Int $id ): ?Collection {

        return $this->model->where( 'base_sheet_id', $id )
                           ->get();
    }

    public function store( Array $attributes ): PressSheet {

        return $this->model->create( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->model->where( 'id', $id )
                           ->update( $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->model->find( $id )->delete();
    }
}
