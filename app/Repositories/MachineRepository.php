<?php

namespace App\Repositories;

use App\Machine;
use Illuminate\Database\Eloquent\Collection;

class MachineRepository
{
    private $model;

    public function __construct( Machine $model ) {

        $this->model = $model;
    }

    public function getAll(): ?Collection { //dd('ovde');

        return $this->model->orderBy( 'name', 'asc')->get();
    }

    public function getById( Int $id ): ?Machine {

        return $this->model->with('pressSheets')->find( $id );
    }

    public function store( Array $attributes ): Machine {

        return $this->model->create( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->model->where( 'id', $id )
                           ->update( $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->model->find( $id )->delete();
    }

    public function sync( Machine $machine, String $relation, Array $ids ): array {
        
        return $machine->$relation()->sync( $ids );
    }
}
