<?php

namespace App\Repositories;

use App\Client;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class ClientRepository
{
    private $model;

    public function __construct( Client $model ) {

        $this->model = $model;
    }

    public function getAll(): ?Collection {

        return $this->model->orderBy( 'contact_name', 'asc' )->get();
    }

    public function getAllPaginated( String $order, String $by, Int $per_page ): ?LengthAwarePaginator {

        return $this->model->orderBy($by, $order)->paginate($per_page);
    }

    public function getById( Int $id ): ?Client {

        return $this->model->find( $id );
    }

    public function store( Array $attributes ): Client {

        return $this->model->create( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->model->where( 'id', $id )
                           ->update( $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->model->find( $id )->delete();
    }
}
