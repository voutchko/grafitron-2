<?php

namespace App\Repositories;

use App\Paper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class PaperRepository
{
    private $model;

    public function __construct( Paper $model ) {

        $this->model = $model;
    }

    public function getAll(): ?Collection {

        return $this->model->orderBy( 'name', 'asc')->all();
    }

    public function getById( Int $id ): ?Paper {

        return $this->model->with('baseSheet')->find( $id );
    }

    public function getByBaseSheet( Int $id ): ?Collection {

        return $this->model->where( 'base_sheet_id', $id )
                           ->get();
    }

    public function getAllPaginated( String $order, String $by, Int $per_page ): ?LengthAwarePaginator {

        return $this->model->orderBy($by, $order)->with('baseSheet')->paginate($per_page);
    }

    public function store( Array $attributes ): Paper {

        return $this->model->create( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->model->where( 'id', $id )
                           ->update( $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->model->find( $id )->delete();
    }
}
