<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'contact_name',
        'business_name',
        'address',
        'notes',
        'pib',
        'email',
        'tel',
        'mb'
    ];
}
