<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    const TABLE = 'papers';
    
    protected $table = self::TABLE; 

    protected $fillable = [
        'name',
        'description',
        'price_per_kg',
        'price_per_sheet',
        'specific_weight',
        'base_sheet_id'
    ];

    public function baseSheet()
	{
	    return $this->belongsTo(BaseSheet::class);
	}
}
