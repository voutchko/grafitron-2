<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PressSheet extends Model
{
    const TABLE = 'press_sheets';
    
    protected $table = self::TABLE; 

    protected $fillable = [
        'name',
        'x',
        'y',
        'base_sheet_id',
        'how_many' 
    ];

	public function baseSheet()
	{
	    return $this->belongsTo(BaseSheet::class);
	}
}
