<?php

if (! function_exists('formatNumber')) {
    function formatNumber ( $number, $decimals = 0 ) {
    	if ($number == (int) $number) {
    		$decimals = 0;
    	}

        return number_format($number, $decimals, ",", "."); 
    }
}