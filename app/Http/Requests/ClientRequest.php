<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique_pib = ( $this->id ?? '' ) ? 'unique:clients,pib,'.$this->id : 'unique:clients,pib';
        $unique_mb = ( $this->id ?? '' ) ? 'unique:clients,mb,'.$this->id : 'unique:clients,mb';
        $unique_email = ( $this->id ?? '' ) ? 'unique:clients,email,'.$this->id : 'unique:clients,email';
        
        return [
            'contact_name'  => 'required',
            'business_name'  => 'required',
            'pib'     => ['required', 'digits:9', 'numeric', $unique_pib],
            'mb'     => ['nullable', 'digits:8', 'numeric', $unique_mb],
            'email'     => ['required', 'email', $unique_email]
        ];
    }
}
