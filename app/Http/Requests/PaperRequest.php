<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ( $this->id ?? '' ) ? 'unique:papers,name,'.$this->id : 'unique:papers,name';

        return [
            'name'              => ['required', $unique],
            'price_per_kg'      => 'required_if:price_per_sheet,""|numeric|nullable',
            'price_per_sheet'   => 'required_if:price_per_kg,""|numeric|nullable',
            'specific_weight'   => 'required|regex:/^(\d*[,]{0,1}){1,}$/i',
            'base_sheet_id'     => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if ( app()->getLocale() == 'sr') {
            return [
                'regex'         => 'Specifične težine papira unesite samo sa zarezima između!',
                'required_if'   => 'Jedna od cena mora biti uneta!',
            ];
        }

        return [
            'regex'         => 'Specific weights shoud be only separated by commas',
            'required_if'   => 'At least one price is mandatory',
        ];
    }
}
