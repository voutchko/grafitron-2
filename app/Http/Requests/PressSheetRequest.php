<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PressSheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ( $this->id ?? '' ) ? 'unique:press_sheets,name,'.$this->id : 'unique:press_sheets,name';

        return [
            'name'      => ['required', $unique],
            'x'         => 'required|numeric',
            'y'         => 'required|numeric',
            'how_many'  => 'required|numeric'
        ];
    }
}
