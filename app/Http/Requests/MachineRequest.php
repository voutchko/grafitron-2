<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MachineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = ( $this->id ?? '' ) ? 'unique:machines,name,'.$this->id : 'unique:machines,name';

        return [
            'name'              => ['required', $unique],
            'price_per_tausand' => 'required_if:price_per_hour,""|numeric|nullable',
            'price_per_hour'    => 'required_if:price_per_tausand,""|numeric|nullable',
            'colour_no'         => 'required|numeric'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if ( app()->getLocale() == 'sr') {
            return [
                'required_if'   => 'Jedna od cena mora biti uneta!',
            ];
        }

        return [
            'required_if'   => 'At least one price is mandatory',
        ];
    }
}
