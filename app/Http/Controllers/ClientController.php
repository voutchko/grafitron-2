<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Services\ClientService;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    const ATTRIBUTES = [
        'contact_name',
        'business_name',
        'address',
        'notes',
        'pib',
        'email',
        'tel',
        'mb'
    ];
    const PER_PAGE = 50;
    const ORDER = 'desc';
    const BY = 'contact_name';

    private $clientService;
    
    public function __construct( ClientService $clientService ) {
        
        $this->middleware(['auth', 'verified']);

        $this->clientService = $clientService;
    }
    
    public function index( Request $request ) {

        $order = $request->order ?? self::ORDER;
        $by = $request->by ?? self::BY;

        $clients = $this->clientService->getAllPaginated( $order, $by, self::PER_PAGE );

        return view( 'clients.index', compact( 'clients', 'order', 'by' ) );
    }

    public function create() {
        
        return view('clients.create');
    }

    public function store( ClientRequest $request ) {

        $attributes = $request->only( self:: ATTRIBUTES );
        
        try {

            $this->clientService->store( $attributes );

        } catch (\Exception $exception) {
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('New client data saved');

        return redirect()->back()->with([
                    'status'  => 'success',
                    'message' => $message
                ])->withInput();
    }

    public function edit( Int $id ) {
        
        $client = $this->clientService->getById( $id );

        return view( 'clients.edit', compact('client') );
    }

    public function update( ClientRequest $request, Int $id ) {
        
        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->clientService->update( $id, $attributes );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();

        }

        return redirect()->back()->withInput();
    }

    public function destroy( Int $id ) {
        
        try {

            $this->clientService->delete( $id );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('Client deleted.');

        return redirect()->route('clients.index')->with([
                    'status'  => 'success',
                    'message' => $message
                ]);
    }
}
