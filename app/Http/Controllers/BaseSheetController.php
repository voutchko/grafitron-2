<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseSheetRequest;
use App\Services\BaseSheetService;
use Illuminate\Http\Request;
use App\BaseSheet;

class BaseSheetController extends Controller
{
    const ATTRIBUTES = [
        'name',
        'x',
        'y',
    ];

    private $baseSheetService;
    
    public function __construct( BaseSheetService $baseSheetService ) {
        
        $this->middleware(['auth', 'verified']);

        $this->baseSheetService = $baseSheetService;
    }
    
    public function index() {

        $base_sheets = $this->baseSheetService->getAll();

        return view( 'base-sheets.index', compact( 'base_sheets' ) );
    }

    public function create() {

        return view( 'base-sheets.create' );
    }

    public function store( BaseSheetRequest $request ) {

        $attributes = $request->only(self::ATTRIBUTES);

        try {

           $this->baseSheetService->store( $attributes );

        } catch (\Exception $exception) {
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        return redirect()->route('base-sheets.index');
    }

    public function edit( Int $id ) {

        if ( $baseSheet = $this->baseSheetService->getById( $id ) ) {

            return view( 'base-sheets.edit', compact( 'baseSheet' ) );
        }

        return redirect()->route( 'base-sheets.index' );
    }

    public function update( BaseSheetRequest $request, Int $id ) {

        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->baseSheetService->update( $id, $attributes );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();

        }

        return redirect()->back()->withInput();
    }

    public function destroy( Int $id ) {

        if ( $this->baseSheetService->hasPressSheets( $id ) ) {

            $error = __("Base sheet can't be deleted if it has press sheets or papers defined.");

            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => $error
                ])
                ->withInput();
        }

        try {

            $this->baseSheetService->delete( $id );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('Sheet deleted.');

        return redirect()->route('base-sheets.index')->with([
                    'status'  => 'success',
                    'message' => $message
                ]);
    }
}
