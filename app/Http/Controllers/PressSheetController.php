<?php

namespace App\Http\Controllers;

use App\Http\Requests\PressSheetRequest;
use App\PressSheet;
use App\Services\BaseSheetService;
use App\Services\PressSheetService;
use Illuminate\Http\Request;

class PressSheetController extends Controller
{
    const ATTRIBUTES = [
        'name',
        'x',
        'y',
        'base_sheet_id',
        'how_many'
    ];

    private $pressSheetService;
    private $basesSheetService;
    
    public function __construct( PressSheetService $pressSheetService, BaseSheetService $baseSheetService ) {
        
        $this->middleware(['auth', 'verified']);

        $this->pressSheetService = $pressSheetService;
        $this->baseSheetService = $baseSheetService;
    }
    
    public function index() {

        $base_sheets = $this->baseSheetService->getAll();

        if ( $base_sheets->count() ) {

            $base_sheet_selected = $base_sheets->first();
            $press_sheets = $this->pressSheetService->getByBaseSheet( $base_sheet_selected->id );

            return view( 'press-sheets.index', compact( 'base_sheets', 'base_sheet_selected', 'press_sheets' ) );

        }

        $error = __('You must define at least one base sheet first.');

        return redirect()->route('home')->with([
                    'status'  => 'danger',
                    'message' => $error
                ]);
    }

    public function filter(Request $request) {

        $base_sheets = $this->baseSheetService->getAll();

        if ( $base_sheets->count() ) {

            $base_sheet_selected = $base_sheets->where('id', '=', $request->base_sheet)->first();
            $press_sheets = $this->pressSheetService->getByBaseSheet( $base_sheet_selected->id );

            return view( 'press-sheets.index', compact( 'base_sheets', 'base_sheet_selected', 'press_sheets' ) );

        }

        $error = __('You must define at least one base sheet first.');

        return redirect()->route('home')->with([
                    'status'  => 'danger',
                    'message' => $error
                ]);
    }

    public function create(Int $base_sheet_id) {

        $base_sheets = $this->baseSheetService->getAll();

        return view('press-sheets.create', compact('base_sheets', 'base_sheet_id'));
    }

    public function store(PressSheetRequest $request) {

        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->pressSheetService->store( $attributes );

        }  catch (\Exception $exception) {
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

       return redirect()->route('press-sheets.index');
    }

    public function edit( Int $id ) {

        $base_sheets = $this->baseSheetService->getAll();
        
        if ( $press_sheet = $this->pressSheetService->getById( $id ) ) {

            return view( 'press-sheets.edit', compact( 'press_sheet', 'base_sheets' ) );
        }

        return redirect()->route( 'press-sheets.index' );
    }

    public function update( PressSheetRequest $request, Int $id ) {
        
        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->pressSheetService->update( $id, $attributes );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        return redirect()->back();
    }

    public function destroy( Int $id ) {
        
        try {

            $this->pressSheetService->delete( $id );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('Sheet deleted.');

        return redirect()->route('press-sheets.index')->with([
                    'status'  => 'success',
                    'message' => $message
                ]);
    }
}
