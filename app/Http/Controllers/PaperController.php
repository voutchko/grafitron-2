<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaperRequest;
use App\Paper;
use App\Services\BaseSheetService;
use App\Services\PaperService;
use Illuminate\Http\Request;

class PaperController extends Controller
{
    const ATTRIBUTES = [
        'name',
        'description',
        'price_per_kg',
        'price_per_sheet',
        'specific_weight',
        'base_sheet_id'
    ];
    const PER_PAGE = 50;
    const ORDER = 'asc';
    const BY = 'name';

    private $basesSheetService;
    private $paperService;
    
    public function __construct( BaseSheetService $baseSheetService, PaperService $paperService ) {
        
        $this->middleware(['auth', 'verified']);

        $this->baseSheetService = $baseSheetService;
        $this->paperService = $paperService;
    }
    
    public function index( Request $request ) {

        $base_sheets = $this->baseSheetService->getAll();

        if ( $base_sheets->count() ) {

            $order = $request->order ?? self::ORDER;
            $by = $request->by ?? self::BY;
            $papers = $this->paperService->getAllPaginated( $order, $by, self::PER_PAGE );

            return view('papers.index', compact('papers', 'order', 'by'));
        }

        $error = __('You must define at least one base sheet first.');

        return redirect()->route('home')->with([
                    'status'  => 'danger',
                    'message' => $error
                ]);
    }

    public function create() {
        
        $base_sheets = $this->baseSheetService->getAll();

        return view('papers.create', compact('base_sheets'));
    }

    public function store( PaperRequest $request ) {
        
        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->paperService->store( $attributes );

        }  catch (\Exception $exception) {
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

       return redirect()->route('papers.index');
    }

    public function edit( Int $id ) {

        $base_sheets = $this->baseSheetService->getAll();
        
        if ( $paper = $this->paperService->getById( $id ) ) {

            return view( 'papers.edit', compact( 'paper', 'base_sheets' ) );
        }

        return redirect()->route( 'papers.index' );
    }

    public function update( PaperRequest $request, Int $id ) {
        
        $attributes = $request->only(self::ATTRIBUTES);

        try {

            $this->paperService->update( $id, $attributes );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        return redirect()->back();
    }

    public function destroy( Int $id ) {
        
        try {

            $this->paperService->delete( $id );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('Paper deleted.');

        return redirect()->route('papers.index')->with([
                    'status'  => 'success',
                    'message' => $message
                ]);
    }
}
