<?php

namespace App\Http\Controllers;

use App\Http\Requests\MachineRequest;
use App\Machine;
use App\Services\MachineService;
use App\Services\PressSheetService;
use Illuminate\Http\Request;

class MachineController extends Controller
{
    const ATTRIBUTES = [
        'name',
        'description',
        'work_and_turn',
        'colour_no',
        'makulatur',
        'plate_price',
        'start_price',
        'price_per_tausand',
        'start_price_per_hour',
        'price_per_hour ',
        'start_time',
        'sheets_per_hour'
    ];

    private $machineService;
    private $pressSheetService;
    
    public function __construct( MachineService $machineService, PressSheetService $pressSheetService ) {
        
        $this->middleware(['auth', 'verified']);

        $this->machineService = $machineService;
        $this->pressSheetService = $pressSheetService;
    }
    
    public function index() {

        $machines = $this->machineService->getAll();

        return view('machines.index', compact('machines'));
    }

    public function create() {
        
        $press_sheets = $this->pressSheetService->getAll();

        return view('machines.create', compact('press_sheets'));
    }

    public function store( MachineRequest $request ) {
        
        $attributes = $request->only(self::ATTRIBUTES);
        $relations = $request->only('press_sheets');

        try {

            $this->machineService->store( $attributes, $relations );

        }  catch (\Exception $exception) {
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

       return redirect()->route('machines.index');
    }

    public function edit( Int $id ) {
        
        $press_sheets = $this->pressSheetService->getAll();
        $machine = $this->machineService->getById( $id );

        return view('machines.edit', compact('machine', 'press_sheets'));
    }

    public function update( MachineRequest $request, Int $id ) {
        
        $attributes = $request->only(self::ATTRIBUTES);
        $relations = $request->only('press_sheets');

        try {

            $this->machineService->update( $id, $attributes, $relations );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        return redirect()->back();
    }

    public function destroy( Int $id) {
        
        try {

            $this->machineService->delete( $id );

        } catch (Exception $exception) {
            
            return redirect()->back()
                ->with([
                    'status'  => 'danger',
                    'message' => 'Error: ' . $exception->getMessage()
                ])
                ->withInput();
        }

        $message = __('Press deleted.');

        return redirect()->route('machines.index')->with([
                    'status'  => 'success',
                    'message' => $message
                ]);
    }
}
