<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    const TABLE = 'machines';
    const PRESS_SHEET_PIVOT_TABLE = 'machine_press_sheet';
    
    protected $table = self::TABLE; 

    protected $fillable = [
        'name',
        'description',
        'work_and_turn',
        'colour_no',
        'makulatur',
        'plate_price',
        'start_price',
        'price_per_tausand',
        'start_price_per_hour',
        'price_per_hour ',
        'start_time',
        'sheets_per_hour'
    ];

    public function pressSheets()
    {
        return $this->belongsToMany(PressSheet::class, self::PRESS_SHEET_PIVOT_TABLE);
    }

    public static function boot() {
        
        parent::boot();

        static::deleting(function($machine) { // before delete() method call this
             $machine->pressSheets()->detach();
        });
    }
}
