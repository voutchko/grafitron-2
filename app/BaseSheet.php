<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseSheet extends Model
{
    const TABLE = 'base_sheets';
    
    protected $table = self::TABLE; 

    protected $fillable = [
        'name',
        'x',
        'y'
    ];

    public function pressSheet()	{

	    return $this->hasMany(PressSheet::class);
	}

    public function paper() {
    	
        return $this->hasMany(Paper::class);
    }
}
