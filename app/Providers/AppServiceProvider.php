<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // For those running MariaDB or older versions of MySQL than MySQL v5.7.7 , or on Windows:
        // https://laravel-news.com/laravel-5-4-key-too-long-error

        if ( env('MYSQL_ERR_1071', false) ) {

            Schema::defaultStringLength(191);
        }
    }
}
