<?php

namespace App\Services;

use App\Machine;
use App\Repositories\MachineRepository;
use Illuminate\Database\Eloquent\Collection;

class MachineService
{
    private $repository;

    public function __construct( MachineRepository $repository ) {

        $this->repository = $repository;
    }

    public function getAll(): ?Collection {

        return $this->repository->getAll();
    }

    public function getById( Int $id ): ?Machine {

        return $this->repository->getById( $id );
    }

    public function store( Array $attributes, Array $relations ): Machine {

        $machine = $this->repository->store( $attributes );

        $this->syncs( $machine, $relations );

        return $machine;
    }

    public function update( Int $id, Array $attributes, Array $relations ): Bool {

        if ( $this->repository->update( $id, $attributes ) ) {

            $machine = $this->repository->getById( $id );

            $this->syncs( $machine, $relations );

            return true;
        }

        return false;
    }

    public function delete( Int $id ): Bool {

        return $this->repository->delete( $id );
    }

    private function syncs( Machine $machine, Array $relations ) {

        $pressSheetIds = !empty($relations['press_sheets']) ? $relations['press_sheets'] : [];
        
        $this->repository->sync( $machine, 'pressSheets', $pressSheetIds );
    }
}
