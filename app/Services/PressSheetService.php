<?php

namespace App\Services;

use App\PressSheet;
use App\Repositories\PressSheetRepository;
use Illuminate\Database\Eloquent\Collection;

class PressSheetService
{
    private $repository;

    public function __construct( PressSheetRepository $repository ) {

        $this->repository = $repository;
    }

    public function getAll(): ?Collection {

        return $this->repository->getAll();
    }

    public function getById( Int $id ): ?PressSheet {

        return $this->repository->getById( $id );
    }

    public function getByBaseSheet( Int $id ): ?Collection {

        return $this->repository->getByBaseSheet( $id );
    }

    public function store( Array $attributes ): PressSheet {

        return $this->repository->store( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->repository->update( $id, $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->repository->delete( $id );
    }
}
