<?php

namespace App\Services;

use App\Client;
use App\Repositories\ClientRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class ClientService
{
    private $repository;

    public function __construct( ClientRepository $repository ) {

        $this->repository = $repository;
    }

    public function getAll(): ?Collection {

        return $this->repository->getAll();
    }

    public function getAllPaginated( String $order, String $by, Int $per_page ): ?LengthAwarePaginator {

        return $this->repository->getAllPaginated( $order, $by, $per_page );
    }

    public function getById( Int $id ): ?Client {

        return $this->repository->getById( $id );
    }

    public function store( Array $attributes ): Client {

        return $this->repository->store( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->repository->update( $id, $attributes );
    }

    public function delete( Int $id ): Bool {

        return $this->repository->delete( $id );
    }
}
