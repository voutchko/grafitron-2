<?php

namespace App\Services;

use App\BaseSheet;
use App\Repositories\BaseSheetRepository;
use Illuminate\Database\Eloquent\Collection;

class BaseSheetService
{
    private $repository;

    public function __construct( BaseSheetRepository $repository ) {

        $this->repository = $repository;
    }

    public function getAll(): ?Collection {

        return $this->repository->getAll();
    }

    public function getById( Int $id ): ?BaseSheet {

        return $this->repository->getById( $id );
    }

    public function store( Array $attributes ): BaseSheet {

        return $this->repository->store( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->repository->update( $id, $attributes );
    }

    public function hasPressSheets( Int $id ): Bool {

        return $this->repository->hasPressSheets( $id );
    }

    public function delete( Int $id ): Bool {

        return $this->repository->delete( $id );
    }
}
