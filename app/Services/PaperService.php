<?php

namespace App\Services;

use App\Paper;
use App\Repositories\PaperRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class PaperService
{
    private $repository;

    public function __construct( PaperRepository $repository ) {

        $this->repository = $repository;
    }

    public function getAll(): ?Collection {

        return $this->repository->getAll();
    }

    public function getById( Int $id ): ?Paper {

        return $this->repository->getById( $id );
    }

    public function getAllPaginated( String $order, String $by, Int $per_page ): ?LengthAwarePaginator {

        return $this->repository->getAllPaginated( $order, $by, $per_page );
    }

    public function store( Array $attributes ): Paper {

        return $this->repository->store( $attributes );
    }

    public function update( Int $id, Array $attributes ): Bool {

        return $this->repository->update( $id, $attributes );
    }

    public function hasPressSheets( Int $id ): Bool {

        return $this->repository->hasPressSheets( $id );
    }

    public function delete( Int $id ): Bool {

        return $this->repository->delete( $id );
    }
}
